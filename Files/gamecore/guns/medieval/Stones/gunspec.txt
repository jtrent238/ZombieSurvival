;STONES weapon spec (Should everything else fail!)
;Model, textures and animations by Jonathan Fletcher
;jonfletc@gmail.com


;GUN Model File Requirements

;GUN Settings

muzzleflash    = 0
muzzlesize     = 0
brass          = 0
smoke          = 0
;flak          = FantasyPack\STONES
damage         = 35
damagetype     = 0
scorchtype     = 4
reloadqty      = 0
iterate        = 0
accuracy       = 0

; GUN HUD (0-colt,1-magnum,2-uzi,3-shotgun,4-rifle,5-rpg,6-grenade,7-bow,8-mace,9-stone,10-sword)
statuspanelcode = 9

;GUN Visuals

textured      =
effect        = 
transparency  = 1
weapontype    = 1

;GUN Sounds

sound1	       = fire.wav
sound2         = retrieve.wav
sound3         = putaway.wav
sound4         = idle.wav

fireloop       = 0

;HUD muzzleflash position

horiz          = 0
vert           = 0
forward        = 4
alignx         = 0
aligny         = 0
alignz         = 13

;HUD animation frames list

keyframe ratio = 1
select         = 1,35
Idle           = 36,59
Move           = 62,89
start fire     = 91,106
end fire       = 107,123
putaway	       = 124,155

;HUD sound frames list

soundframes    = 3
sframe0        = 104,1
sframe1        = 6,2
sframe2        = 130,3
